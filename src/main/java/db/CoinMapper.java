package db;

import entity.CoinDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface CoinMapper {
    @Select("select coin.amount from coin where type=#{type}")
    int getAmount(String type);

    @Update("update coin set amount=#{amount} where type=#{type}")
    void update(CoinDao coin);
}
