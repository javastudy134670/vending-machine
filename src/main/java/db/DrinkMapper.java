package db;

import entity.DrinkDao;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DrinkMapper {
    @Select("select * from drink")
    List<DrinkDao> selectAll();

    @Select("select * from drink where name = #{name}")
    DrinkDao selectByName(String name);

    @Update("update drink set price = #{price}, allowance = #{allowance} where name = #{name}")
    void update(DrinkDao drink);

    @Delete("delete from drink where name= #{name}")
    void delete(DrinkDao drink);

    @Insert("insert into drink (name, price, allowance) values (#{name}, #{price}, #{allowance})")
    void insert(DrinkDao drink);
}
