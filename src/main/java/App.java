import gui.MainFrame;

import javax.swing.*;
import java.util.logging.Logger;

public class App {
    public static final Logger logger = Logger.getLogger("app");
    public static void main(String[] args) {
        MainFrame mainFrame = MainFrame.getInstance();
        mainFrame.setVisible(true);
        logger.info("The program was successfully launched.");
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
