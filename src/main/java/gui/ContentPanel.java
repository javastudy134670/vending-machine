package gui;

import javax.swing.*;
import java.awt.*;

public class ContentPanel extends JPanel {
    private ContentPanel() {
        setLayout(new BorderLayout());
        add(BalancePanel.getInstance(), BorderLayout.NORTH);
        add(AddNewButton.getInstance(),BorderLayout.CENTER);
        add(CommodityViewPanel.getInstance(), BorderLayout.SOUTH);
    }

    private static final ContentPanel contentPanel = new ContentPanel();

    public static ContentPanel getInstance() {
        return contentPanel;
    }

}
