package gui;

import service.BalanceService;

import java.util.logging.Logger;

public class DimeCoinPanel extends BaseCoinPanel {

    protected DimeCoinPanel() {
        super("Dime");
    }

    @Override
    protected void updateValue(int num) {
        BalanceService.setCountDime(num);
        revalidateText();
        BalancePanel.getInstance().revalidateText();
        Logger.getLogger("app").info("'%s' was reset to %d".formatted(name,num));
    }

    @Override
    protected void revalidateText() {
        view.setText("%s Count: %d".formatted(name, BalanceService.countDime()));
        revalidate();
    }

    private static final DimeCoinPanel dimeCoinPanel = new DimeCoinPanel();

    public static DimeCoinPanel getInstance() {
        return dimeCoinPanel;
    }
}
