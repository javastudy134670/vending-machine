package gui;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

/**
 * 滚动栏的UI设计
 */
public class MyBasicScrollBarUI extends BasicScrollBarUI {
    /**
     * 改写getPreferredSize方法，返回这个滚动栏的大小
     *
     * @param c the <code>JScrollBar</code> that's delegating this method to us
     * @return 返回这个滚动栏的大小
     */
    @Override
    public Dimension getPreferredSize(JComponent c) {
        return new Dimension(10, 10);
    }

    /**
     * 改写paintTrack方法，绘制滚动轨道，变成灰色轨道
     *
     * @param g           the graphics 图形对象
     * @param c           the component 母组件
     * @param trackBounds the track bounds 轨道的Bounds（即大小、位置）
     */
    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        Color trackColor = new Color(0, 0, 0, 70);//这个颜色可以改成自定义颜色
        g.setColor(trackColor);
        g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
    }

    /**
     * 改写pintThumb方法，绘制滑块
     *
     * @param g           the graphics 图形对象
     * @param c           the component 母组件
     * @param thumbBounds the thumb bounds 滑块的Bounds
     */
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        Color thumbColor = new Color(0, 0, 0, 150);//这个颜色可以自定义
        g.setColor(thumbColor);
        g.fillRect(thumbBounds.x, thumbBounds.y, thumbBounds.width, thumbBounds.height);
    }

    /**
     * 不要这个按钮
     *
     * @param orientation the orientation
     * @return 不存在的按钮
     */
    @Override
    protected JButton createDecreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(0, 0);
            }
        };
    }

    /**
     * 不要这个按钮
     *
     * @param orientation the orientation
     * @return 不存在的按钮
     */
    @Override
    protected JButton createIncreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(0, 0);
            }
        };
    }
}
