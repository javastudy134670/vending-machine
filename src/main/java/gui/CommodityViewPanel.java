package gui;

import db.DBUtil;
import db.DrinkMapper;
import entity.DrinkDao;
import org.apache.ibatis.session.SqlSession;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.image.DirectColorModel;
import java.util.logging.Logger;

public class CommodityViewPanel extends JPanel {

    public void reFresh() {
        try (SqlSession session = DBUtil.sqlSessionFactory.openSession()) {
            model.clear();
            model.addAll(session.getMapper(DrinkMapper.class).selectAll());
            revalidate();
        }
    }

    private CommodityViewPanel() {
        JList<DrinkDao> list = new JList<>(model);
        JScrollPane scrollPane = new JScrollPane(list);
        TitledBorder titledBorder = new TitledBorder("DRINK PRICE LIST");
        JPanel options = new JPanel();
        JButton editButton = new JButton("Edit");
        JButton sellButton = new JButton("Sell");
        JButton deleteButton = new JButton("Delete");
        list.setBackground(Color.gray);
        scrollPane.setBackground(Color.gray);
        sellButton.setBackground(Color.green);
        sellButton.setForeground(Color.white);
        options.setBackground(Color.gray);
        editButton.setBackground(Color.cyan);
        editButton.setForeground(Color.white);
        deleteButton.setBackground(Color.red);
        deleteButton.setForeground(Color.white);
        sellButton.addActionListener(e -> sellPerformed(list.getSelectedValue()));
        editButton.addActionListener(e -> editPerformed(list.getSelectedValue()));
        deleteButton.addActionListener(e -> deletePerformed(list.getSelectedValue()));
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        scrollPane.setPreferredSize(new Dimension(300, 250));
        scrollPane.getVerticalScrollBar().setUI(new MyBasicScrollBarUI());
        list.setCellRenderer((list1, value, index, isSelected, cellHasFocus) -> {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JLabel nameLabel = new JLabel(value.name());
            JLabel priceLabel = new JLabel("￥%.1f".formatted(value.price() / 10.0));
            JLabel amountLabel = new JLabel("stock left: %d".formatted(value.allowance()));
            nameLabel.setForeground(Color.cyan);
            priceLabel.setForeground(Color.green);
            amountLabel.setForeground(Color.white);
            panel.setBackground(Color.darkGray);
            panel.setBorder(BorderFactory.createLineBorder(Color.lightGray));
            panel.add(nameLabel);
            panel.add(priceLabel);
            panel.add(amountLabel);
            if (isSelected) panel.setBackground(Color.gray);
            return panel;
        });
        this.setBorder(titledBorder);
        this.setLayout(new BorderLayout());
        this.add(scrollPane, BorderLayout.NORTH);
        this.add(options, BorderLayout.SOUTH);
        this.setBackground(Color.gray);
        options.add(editButton, BorderLayout.WEST);
        options.add(deleteButton, BorderLayout.CENTER);
        options.add(sellButton, BorderLayout.EAST);
        reFresh();
    }

    private void editPerformed(DrinkDao drinkDao) {
        if (drinkDao == null) {
            JOptionPane.showMessageDialog(null, "还未选择产品", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        JTextField allowanceField = new JTextField(10);
        JTextField priceField = new JTextField(10);
        allowanceField.setText(Integer.toString(drinkDao.allowance()));
        priceField.setText(Integer.toString(drinkDao.price()));
        Object[] message = {
                "数量", allowanceField,
                "价格", priceField,
        };
        int price;
        int allowance;
        int option = JOptionPane.showConfirmDialog(null, message, "修改%s饮料信息".formatted(drinkDao.name()), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            try {
                price = Integer.parseInt(priceField.getText());
                if (price <= 0) {
                    JOptionPane.showMessageDialog(null, "价格必须是正整数，单位是“角”", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(MainFrame.getInstance(), "价格必须是正整数，单位是“角”", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                allowance = Integer.parseInt(allowanceField.getText());
                if (allowance <= 0) {
                    JOptionPane.showMessageDialog(MainFrame.getInstance(), "数量必须是正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(MainFrame.getInstance(), "数量必须是正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try (SqlSession session = DBUtil.sqlSessionFactory.openSession(true)) {
                DrinkDao newDrink = new DrinkDao(drinkDao.name(), price, allowance);
                session.getMapper(DrinkMapper.class).update(newDrink);
                CommodityViewPanel.getInstance().reFresh();
                Logger.getLogger("app").info("update drink " + newDrink);
            }
        }
    }

    private void sellPerformed(DrinkDao drinkDao) {
        if (drinkDao == null) {
            JOptionPane.showMessageDialog(null, "还未选择产品", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        JTextField amountField =new JTextField(10);
        amountField.setText("1");
        Object[] message = {
                "购买数量",
                amountField
        };
        int option = JOptionPane.showConfirmDialog(null,message,"购买%s".formatted(drinkDao.name()),JOptionPane.OK_CANCEL_OPTION);
        if(option==JOptionPane.OK_OPTION){

        }
    }

    private void deletePerformed(DrinkDao drinkDao) {
        if (drinkDao == null) {
            JOptionPane.showMessageDialog(null, "还未选择产品", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int option = JOptionPane.showConfirmDialog(null, "确认删除？", "删除%s饮料系列".formatted(drinkDao.name()), JOptionPane.OK_CANCEL_OPTION);
        if(option==JOptionPane.OK_OPTION) {
            try (SqlSession session = DBUtil.sqlSessionFactory.openSession(true)) {
                session.getMapper(DrinkMapper.class).delete(drinkDao);
                CommodityViewPanel.getInstance().reFresh();
                Logger.getLogger("app").info("delete drink " + drinkDao);
            }
        }
    }

    private static final DefaultListModel<DrinkDao> model = new DefaultListModel<>();

    private static final CommodityViewPanel commodityViewPanel = new CommodityViewPanel();

    public static CommodityViewPanel getInstance() {
        return commodityViewPanel;
    }

}
