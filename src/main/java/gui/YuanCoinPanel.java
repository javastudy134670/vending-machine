package gui;

import service.BalanceService;

import java.util.logging.Logger;

public class YuanCoinPanel extends BaseCoinPanel {

    protected YuanCoinPanel() {
        super("Yuan");
    }

    @Override
    protected void updateValue(int num) {
        BalanceService.setCountYuan(num);
        revalidateText();
        BalancePanel.getInstance().revalidateText();
        Logger.getLogger("app").info("'%s' was reset to %d".formatted(name, num));
    }

    @Override
    protected void revalidateText() {
        view.setText("%s Count: %d".formatted(name, BalanceService.countYuan()));
        revalidate();
    }

    private static final YuanCoinPanel yuanCoinPanel = new YuanCoinPanel();

    public static YuanCoinPanel getInstance() {
        return yuanCoinPanel;
    }
}
