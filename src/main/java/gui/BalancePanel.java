package gui;

import service.BalanceService;

import javax.swing.*;
import java.awt.*;

public class BalancePanel extends JPanel {
    public final JLabel overview = new JLabel();

    private BalancePanel() {
        JPanel topPanel = new JPanel();
        JPanel bottomPanel = new JPanel();
        JPanel gap = new JPanel();
        gap.setBackground(Color.gray);
        overview.setForeground(Color.white);
        topPanel.setBackground(Color.darkGray);
        topPanel.add(overview);
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.add(YuanCoinPanel.getInstance());
        bottomPanel.add(gap);
        bottomPanel.add(DimeCoinPanel.getInstance());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        add(topPanel);
        add(bottomPanel);
        revalidateText();
    }

    public void revalidateText() {
        overview.setText("Balance: ￥%.1f".formatted(BalanceService.countDime() / 10.0 + BalanceService.countYuan()));
        revalidate();
    }

    private static final BalancePanel balancePanel = new BalancePanel();

    public static BalancePanel getInstance() {
        return balancePanel;
    }
}
