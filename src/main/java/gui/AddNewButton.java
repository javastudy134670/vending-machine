package gui;

import db.DBUtil;
import db.DrinkMapper;
import entity.DrinkDao;
import org.apache.ibatis.session.SqlSession;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class AddNewButton extends JButton {
    private AddNewButton() {
        super("Add New Drink");
        setBackground(Color.cyan);
        setForeground(Color.white);
        setFont(new Font(Font.DIALOG, Font.BOLD, 20));
        addActionListener(e -> perform());
    }

    private void perform() {
        JTextField nameField = new JTextField(10);
        JTextField priceField = new JTextField(10);
        JTextField allowanceField = new JTextField(10);
        Object[] message = {
                "饮料名：", nameField,
                "价格/角：", priceField,
                "数量：", allowanceField
        };
        int price;
        int allowance;
        int option = JOptionPane.showConfirmDialog(null, message, "添加新品饮料", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            if (nameField.getText().isBlank()) {
                JOptionPane.showMessageDialog(null, "饮料名称不能为空", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                price = Integer.parseInt(priceField.getText());
                if (price <= 0) {
                    JOptionPane.showMessageDialog(null, "价格必须是正整数，单位是“角”", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "价格必须是正整数，单位是“角”", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                allowance = Integer.parseInt(allowanceField.getText());
                if (allowance <= 0) {
                    JOptionPane.showMessageDialog(null, "数量必须是正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "数量必须是正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try (SqlSession session = DBUtil.sqlSessionFactory.openSession(true)) {
                if (session.getMapper(DrinkMapper.class).selectByName(nameField.getText()) != null) {
                    JOptionPane.showMessageDialog(null, "重复添加", "错误提示", JOptionPane.ERROR_MESSAGE);
                } else {
                    DrinkDao newDrink = new DrinkDao(nameField.getText(), price, allowance);
                    session.getMapper(DrinkMapper.class).insert(newDrink);
                    CommodityViewPanel.getInstance().reFresh();
                    Logger.getLogger("app").info("added a new drink " + newDrink);
                }
            }
        }
    }

    private static final AddNewButton addNewButton = new AddNewButton();

    public static AddNewButton getInstance() {
        return addNewButton;
    }
}
