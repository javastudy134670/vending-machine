package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;

public class MainFrame extends JFrame {
    private MainFrame() {
        setTitle("Auto Vending Machine");
        setBounds(new Rectangle(350, 500));
        setResizable(false);
        setLocationRelativeTo(null);
        setContentPane(ContentPanel.getInstance());
        revalidate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Logger.getLogger("app").info("The program exits normally.");
            }
        });
    }

    private static final MainFrame mainFrame = new MainFrame();

    public static MainFrame getInstance() {
        return mainFrame;
    }

}
