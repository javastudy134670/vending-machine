package gui;

import javax.swing.*;
import java.awt.*;

public abstract class BaseCoinPanel extends JPanel {
    protected final JLabel view = new JLabel();
    protected final String name;

    protected BaseCoinPanel(String name) {
        this.name = name;
        JButton setValue = new JButton("reset");
        setValue.addActionListener(e -> {
            String input = JOptionPane.showInputDialog(MainFrame.getInstance(), "请设置%s硬币数".formatted(name), "输入对话框", JOptionPane.QUESTION_MESSAGE);
            if (input != null && !input.isBlank()) {
                try {
                    int num = Integer.parseInt(input);
                    if (num < 0) {
                        JOptionPane.showMessageDialog(MainFrame.getInstance(), "硬币数不能是负数，请重新输入", "错误提示", JOptionPane.ERROR_MESSAGE);
                    } else {
                        updateValue(num);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(MainFrame.getInstance(), "您输入的不是一个整数，请重新输入", "错误提示", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
//        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        setValue.setBackground(Color.white);
        view.setForeground(Color.GREEN);
        setBackground(Color.gray);
        add(view);
        add(setValue);
//        setBounds(new Rectangle(175,80));
        setPreferredSize(new Dimension(150, 80));
        revalidateText();
    }

    protected abstract void updateValue(int num);

    protected abstract void revalidateText();
}
