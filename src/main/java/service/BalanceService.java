package service;

import db.CoinMapper;
import db.DBUtil;
import entity.CoinDao;
import org.apache.ibatis.session.SqlSession;

/**
 * Balance数据操作API
 */
public class BalanceService {
    public static int countDime() {
        int count;
        try (SqlSession session = DBUtil.sqlSessionFactory.openSession()) {
            count = session.getMapper(CoinMapper.class).getAmount("Dime");
        }
        return count;
    }

    public static int countYuan() {
        int count;
        try (SqlSession session = DBUtil.sqlSessionFactory.openSession()) {
            count = session.getMapper(CoinMapper.class).getAmount("Yuan");
        }
        return count;
    }

    public static boolean modifyCountYuan(int difference) {
        return setCountYuan(countYuan() + difference);
    }

    public static boolean setCountYuan(int value) {
        if (value < 0) return false;
        try (SqlSession session = DBUtil.sqlSessionFactory.openSession(true)) {
            session.getMapper(CoinMapper.class).update(new CoinDao("Yuan", value));
        }
        return true;
    }

    public static boolean modifyCountDime(int difference) {
        return setCountDime(countDime() + difference);
    }

    public static boolean setCountDime(int value) {
        if (value < 0) return false;
        try (SqlSession session = DBUtil.sqlSessionFactory.openSession(true)) {
            session.getMapper(CoinMapper.class).update(new CoinDao("Dime", value));
        }
        return true;
    }
}
