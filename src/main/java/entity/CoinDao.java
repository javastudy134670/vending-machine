package entity;

/**
 * 硬币模型
 *
 * @param type 硬币类型，Yuan 或 Dime
 * @param amount 硬币数量，非负数
 */
public record CoinDao(String type, int amount) {
}
