package entity;

/**
 * 商品模型
 *
 * @param name 饮料名称
 * @param price 饮料价格（单位Dime）
 * @param allowance 饮料余量
 */
public record DrinkDao(String name, int price, int allowance) {
}
