import entity.DrinkDao;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GetSelectedValueFromJList extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;

    private JList<DrinkDao> list;
    private JButton button;

    public GetSelectedValueFromJList() {

        // set flow layout for the frame
        this.getContentPane().setLayout(new FlowLayout());

        DrinkDao[] data = {new DrinkDao("Value 1",1,1),new DrinkDao("Value 2",2,2)};

        list = new JList<>(data);

        list.setCellRenderer((list1, value, index, isSelected, cellHasFocus) -> {
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JLabel nameLabel = new JLabel(value.name());
            JLabel priceLabel = new JLabel("￥%.1f".formatted(value.price() / 10.0));
            JLabel amountLabel = new JLabel("stock left: %d".formatted(value.allowance()));
            JButton sellButton = new JButton("Sell");
            JButton editButton = new JButton("Edit");
            nameLabel.setForeground(Color.cyan);
            priceLabel.setForeground(Color.green);
            amountLabel.setForeground(Color.white);
            sellButton.setBackground(Color.blue);
            sellButton.setForeground(Color.white);
            editButton.setBackground(Color.gray);
            editButton.setForeground(Color.white);
            leftPanel.setBackground(Color.darkGray);
            rightPanel.setBackground(Color.darkGray);
            panel.setBorder(BorderFactory.createLineBorder(Color.gray));
            leftPanel.add(nameLabel);
            leftPanel.add(priceLabel);
            leftPanel.add(amountLabel);
            rightPanel.add(editButton);
            rightPanel.add(sellButton);
            panel.add(leftPanel);
            panel.add(rightPanel);
            sellButton.setEnabled(true);
            editButton.setEnabled(true);
            sellButton.addActionListener(e -> System.out.println("hello sell"));
            editButton.addActionListener(e -> System.out.println("hello edit"));
            return panel;
        });
        button = new JButton("Check");

        button.addActionListener(this);

        // add list to frame
        add(list);
        add(button);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Check")) {
            int index = list.getSelectedIndex();
            System.out.println("Index Selected: " + index);
            var s = list.getSelectedValue();
            System.out.println("Value Selected: " + s);
        }
    }

    private static void createAndShowGUI() {

        //Create and set up the window.

        JFrame frame = new GetSelectedValueFromJList();

        //Display the window.

        frame.pack();

        frame.setVisible(true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {

        //Schedule a job for the event-dispatching thread:

        //creating and showing this application's GUI.

        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                createAndShowGUI();

            }

        });
    }

}