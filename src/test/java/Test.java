import javax.swing.*;
import java.awt.event.*;

public class Test extends JFrame {

    public Test() {
// 创建一个按钮
        JButton button = new JButton("点击弹出输入框");
// 为按钮添加监听器
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
// 弹出一个输入对话框，提示用户输入一个整数
                String input = JOptionPane.showInputDialog(Test.this, "请输入一个整数", "输入对话框", JOptionPane.QUESTION_MESSAGE);
// 判断用户的输入是否为空或取消
                if (input == null || input.isEmpty()) {
// 返回或提示
                    return;
                }
                try {
// 将用户的输入转换为整数
                    int num = Integer.parseInt(input);
// 对整数进行处理，比如打印或其他操作
                    System.out.println("您输入的整数是：" + num);
                } catch (NumberFormatException ex) {
// 如果用户的输入不是整数，给出提示信息
                    JOptionPane.showMessageDialog(Test.this, "您输入的不是一个整数，请重新输入", "错误提示", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
// 将按钮添加到窗口中
        this.add(button);
// 设置窗口的属性
        this.setTitle("测试窗口");
        this.setSize(300, 200);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
// 创建一个测试窗口对象
        new Test();
    }
}
