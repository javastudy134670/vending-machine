import gui.MyBasicScrollBarUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

public class MyFrame extends JFrame {
    private static final String[] ITEMS = {"商品1", "商品2", "商品3", "商品4", "商品5", "商品6"};
    private static final double[] PRICES = {10.0, 20.0, 30.0, 20, 40, 5, 17};

    public MyFrame() {
        // 创建一个包含商品信息和sell按钮的JPanel
        ListCellRenderer<Integer> renderer = new ListCellRenderer<>() {
            @Override
            public Component getListCellRendererComponent(JList list, Integer value, int index, boolean isSelected, boolean cellHasFocus) {
                JPanel panel = new JPanel();
                panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
                JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                JLabel nameLabel = new JLabel(ITEMS[index]);
                JLabel priceLabel = new JLabel("$" + PRICES[index]);
                JLabel amountLabel = new JLabel("stock left: %d".formatted(index));
                JButton sellButton = new JButton("Sell");
                JButton editButton = new JButton("Edit");
                nameLabel.setForeground(Color.cyan);
                priceLabel.setForeground(Color.green);
                amountLabel.setForeground(Color.white);
                sellButton.setBackground(Color.blue);
                sellButton.setForeground(Color.white);
                editButton.setBackground(Color.gray);
                editButton.setForeground(Color.white);
                leftPanel.setBackground(Color.darkGray);
                rightPanel.setBackground(Color.darkGray);
                panel.setBorder(BorderFactory.createLineBorder(Color.gray));
                leftPanel.add(nameLabel);
                leftPanel.add(priceLabel);
                leftPanel.add(amountLabel);
                rightPanel.add(editButton);
                rightPanel.add(sellButton);
                panel.add(leftPanel);
                panel.add(rightPanel);
                return panel;
            }
        };

        // 创建一个JList并设置渲染器
        DefaultListModel<Integer> model = new DefaultListModel<>();
        for (int i = 0; i < ITEMS.length; i++) {
            model.addElement(i);
        }
        JList<Integer> list = new JList<>(model);
        list.setBackground(Color.gray);
        list.setCellRenderer(renderer);

        // 将JList添加到窗口中
        JScrollPane jScrollPane = new JScrollPane(list);
        TitledBorder titledBorder = new TitledBorder("DRINK PRICE LIST");
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        add(jScrollPane);
        jScrollPane.setBackground(Color.gray);
        jScrollPane.setBorder(titledBorder);
        jScrollPane.getVerticalScrollBar().setUI(new MyBasicScrollBarUI());


        // 设置窗口属性
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MyFrame();
    }
}
